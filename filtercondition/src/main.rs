struct FilterCondition<T> {
    condition: T,
}

impl<T: PartialEq> FilterCondition<T> {

    fn is_match(&self, item: &T) -> bool {
        item == &self.condition
    }
}

fn custom_filter<T>(collection: &Vec<T>, filter_condition: &FilterCondition<T>) -> Vec<T>
where
    T: PartialEq + Clone, // We specify constraints for T using trait bounds (PartialEq and Clone).
{
    collection
        .iter()
        .cloned()
        // The `filter()` method takes a closure that determines whether an element should be included in the filtered collection.
        .filter(|item| filter_condition.is_match(item))
        // The `collect()` method is used to collect the filtered elements and create a new vector.
        .collect()
}

fn main() {
    let numbers = vec![1, 2, 3, 2, 4, 2, 5, 5, 3];
    let mut select: String;

    
    let filter_condition = FilterCondition { condition: 3 };

    let filtered_numbers = custom_filter(&numbers, &filter_condition);

    println!("Filtered numbers: {:?}", filtered_numbers);
}
