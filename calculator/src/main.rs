//use std::io::{self, stdin, stdout, Write};
use std::io::{self, Write};

enum Operate {
    Add(f64, f64),
    Subtract(f64, f64),
    Multiply(f64, f64),
    Divide(f64, f64),
}

fn calculate(operation: Operate) -> f64 {
    match operation {
        Operate::Add(num1, num2) => num1 + num2,
        Operate::Subtract(num1, num2) => num1 - num2,
        Operate::Multiply(num1, num2) => num1 * num2,
        Operate::Divide(num1, num2) => num1 / num2,
    }
}

fn main() {
    let mut num1: String = String::new();
    let mut num2: String = String::new();
    let mut select: String = String::new();

    loop {
        // read_line function don't clear String, append in the last
        select.clear();
        num1.clear();
        num2.clear();

        print!("Please select operation (1.Add,2.Subtract,3.Multiply,4.Divide):");
        io::stdout().flush().expect("Çıkış tamponu boşaltılamadı");
        io::stdin()
            .read_line(&mut select)
            .expect("Failed to read line");
        print!("first number:");
        io::stdout().flush().expect("Çıkış tamponu boşaltılamadı");
        io::stdin()
            .read_line(&mut num1)
            .expect("Failed to read line");

        print!("second number:");
        io::stdout().flush().expect("Çıkış tamponu boşaltılamadı");
        io::stdin()
            .read_line(&mut num2)
            .expect("Failed to read line");

        let mut result = match select.as_str().trim() {
            "1" => calculate(Operate::Add(
                num1.trim().parse::<f64>().unwrap(),
                num2.trim().parse::<f64>().unwrap(),
            )),
            "2" => calculate(Operate::Subtract(
                num1.trim().parse::<f64>().unwrap(),
                num2.trim().parse::<f64>().unwrap(),
            )),
            "3" => calculate(Operate::Multiply(
                num1.trim().parse::<f64>().unwrap(),
                num2.trim().parse::<f64>().unwrap(),
            )),
            "4" => calculate(Operate::Divide(
                num1.trim().parse::<f64>().unwrap(),
                num2.trim().parse::<f64>().unwrap(),
            )),
            _ => {
                println!("Invalid selection!");
                continue;
            }
        };

        println!("result : {}", result);
    }
}
